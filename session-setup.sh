#!/bin/sh
rm -rf /home/user
mkdir -p /etc/skel/.config/autostart
cp /usr/local/rip-the-system/launch-tor.sh.desktop /etc/skel/.config/autostart
mkdir -p /home/user
rsync -av /etc/skel/./ /home/user/./
chown -R user:user /home/user
cd /usr/local/rip-the-system
php fetch-torbrowser.php
cp /usr/local/rip-the-system/tor-browser_en-US.tar.gz /home/user
cd /home/user
tar xvf tor-browser_en-US.tar.gz
rm /home/user/tor-browser_en-US.tar.gz
chown -R user:user tor-browser_en-US
