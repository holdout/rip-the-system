<?php
$directory = file_get_contents('https://www.torproject.org/dist/torbrowser/linux/');
preg_match('/>(tor-browser-gnu-linux-i686-.*-en-US\.tar\.gz)</', $directory, $matches);
if (file_get_contents('current_version.txt') != $matches[1]) {
  file_put_contents('tor-browser_en-US.tar.gz', file_get_contents('https://www.torproject.org/dist/torbrowser/linux/' . $matches[1]));
  file_put_contents('current_version.txt', $matches[1]);
}
