#!/bin/sh
[ -d /home/user/.gvfs ] && umount -fl /home/user/.gvfs
rm -rf /home/user
mkdir -p /home/user
rsync -av /etc/skel/./ /home/user/./
chown -R user:user /home/user
